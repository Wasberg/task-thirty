import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ElementSchemaRegistry } from '@angular/compiler';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {

  @Input() formTitle: string;

  @Output() registerAttempt: EventEmitter<any> = new EventEmitter();

  private registerResult: any = {
    isRegistered: false,
    message: '',
    password: '',
    confirmPassword: ''
  };

  password(event: any) {
    this.registerResult.password = event.target.value;
  }

  confirmPassword(event: any) {
    this.registerResult.confirmPassword = event.target.value;
  }

  onRegisterClicked() {

    if (this.registerResult.password === this.registerResult.confirmPassword) {
      this.registerResult.message = "You are now registered.";
      this.registerResult.isRegistered = true;
    } else {
      this.registerResult.message = "Your passwords doesn't match.";
      this.registerResult.isRegistered = false;
    }

    this.registerAttempt.emit(this.registerResult);

  }

  constructor() { }

  ngOnInit(): void {
  }

}
