import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'task-thirty';

  onLoginAttempted(loginResult: any): void {
    alert(loginResult.message);
  }

  onRegisterAttempted(registerResult: any): void {
    alert(registerResult.message);
  }
}
